package main

import (
	"fmt"
	"time"
	"io/ioutil"
	"bufio"
	"regexp"
	"os"
	"os/signal"
	"archive/tar"
	"strings"
	"io"
	"bytes"
	"github.com/aws/aws-sdk-go/aws"
    "github.com/aws/aws-sdk-go/aws/credentials"
    "github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	//"github.com/aws/aws-sdk-go/service/sns"
)

var (
	dialplanBase = "/home/h2o/test/aqueduct/freeswitch/dialplan"  // "account-735111/"
	soundsBase   = "/home/h2o/test/sounds/accounts"               // "735111/a"
)

type config struct {
	region string    // us-east-1, ...
	regionID string  // va, or, ...
	appEnv string    // qa7, prod, ...
	bucket string    // vonage-h2o-qa7-va-dialplan-2
	appStack string  // 1, 2, ...
}

/*
Bucket layout in S3:
vonage-h2o-{env}-{regionID}-dialplan-{stackID}

env     : dev, qa7, prod
regionID: va, or, ...
stackID : 1, 2, 3, ...

Key layout in S3:
{hexid}/dialplan  / account-11111.tar, account-11112.tar, ...
{hexid}/sounds    / account-sounds-11111.tar, acccount-sounds-11112.tar, ...

Path layout in tar files:
dialplan: account-{accountID}/*.lua
sounds  : {accountID}/xxx...

*/

func main() {
	fmt.Printf("%s: Starting:\n", time.Now())

	maxThr := 50
	config := loadConfig()

	reDialPlan := regexp.MustCompile("[0-9]+/dialplan/account-([0-9]+).tar")
	reSounds   := regexp.MustCompile("[0-9]+/sounds/account-sounds-([0-9]+).tar")

	chanSignal := make(chan os.Signal)
	signal.Notify(chanSignal, os.Interrupt)

	maxThreadsChan := make(chan bool, maxThr)
	for a := 0; a < maxThr; a++ {
		maxThreadsChan <-true
	}

	//stopSNS := make(chan bool)
	//doneSNS := make(chan bool)

	// Connect to AWS services
	var s3client *s3.S3
	//var snsClient *sns.SNS
	if awsSession, err := NewSession(config.region, "", "", "", "", ""); err == nil {
		s3client = s3.New(awsSession)
		//snsClient = sns.New(awsSession)
	} else {
		fmt.Printf("Could not connect to S3: %s\n", err)
		return
	}

	//startTime := time.Now()

	//go runSNS(stopSNS,doneSNS)
	fmt.Printf("%s: Listing S3:\n", time.Now())
	// Download from S3. If newer than what we have on disk, use it. If on disk, could be from earlier run, or a lost race with the current SNS listener.
	if entries, err := s3List(s3client, config.region, config.bucket, ""); err != nil {
		fmt.Printf("%s: Listing S3: failed - %s\n", time.Now(), err)
		return
	} else {
		fmt.Printf("%s: Listing S3: returned %d items\n", time.Now(), len(entries))
		fmt.Printf("%s: Pulling S3:\n", time.Now())
		for _, entry := range entries {
			<-maxThreadsChan
			//fmt.Printf("Entry: %s - %s\n", entry.Key, entry.File)
			dirPath := localDir(reDialPlan, reSounds, dialplanBase, soundsBase, entry)
			dirTime := localDirTime(dirPath)
			if entry.Modified.After(dirTime) {
				downloadAndExtract(s3client, dirPath, entry)
			} else {
				fmt.Printf("Entry: key(%s) path(%s) file(%s) - have current local, skipping", entry.Key, dirPath, entry.File)
			}
			maxThreadsChan<-true
			/*if gotSignal := <-chanSignal; gotSignal != nil {
				stopSNS <-true
				<-doneSNS
				return
			}
			*/
		}
		fmt.Printf("%s: Pulling S3: completed\n", time.Now())
	}
	/*
	for {
		select {
		case <-chanSignal:
			stopSNS <-true
		case <-doneSNS:
			return
		}
	}
	*/
}

func runSNS(stop, done chan bool) {
	for {
		select {
		case <-stop:
			done<-true
			return
		}
	}
}

func localDir(reDialPlan, reSounds *regexp.Regexp, dialplanBase, soundsBase string, entry *s3Entry) string {
	var dirPath string
	if match := reDialPlan.FindStringSubmatch(entry.Key); len(match) == 2 {
		dirPath = fmt.Sprintf("%s/account-%s", dialplanBase, match[1])
		fmt.Printf("Entry: key(%s) accountID(%s) path(%s) file(%s) - dialplan", entry.Key, match[1], dirPath, entry.File)
	} else if match := reSounds.FindStringSubmatch(entry.Key); len(match) == 2 {
		dirPath = fmt.Sprintf("%s/%s", soundsBase, match[1])
		fmt.Printf("Entry: key(%s) accountID(%s) path(%s) file(%s) - sound", entry.Key, match[1], dirPath, entry.File)
	} else {
		fmt.Printf("Entry: key(%s) - skipping, key does not contain dialplan nor sound\n", entry.Key)
	}
	return dirPath
}

func localDirTime(localDir string) time.Time {
	if stat, err := os.Stat(localDir); err == nil {
		return stat.ModTime()
	}
	return time.Time{}
}

func downloadAndExtract(s3client *s3.S3, dirPath string, entry *s3Entry) {
	pathN := strings.Split(dirPath, "/")
	if tarData, err := s3streamDownload(s3client, entry.Bucket, entry.Key); err == nil {
		os.MkdirAll(dirPath, 0777)
		
		// Each object is actually a tar, so run our function to extract into a map; the key is the tar entry name (the filename), and the value is the file content.
		if tarFiles, err := extractTar(tarData, dirPath); err != nil {
			fmt.Printf("Error extracting tar file %s: %s\n", entry.File, err)
		} else {
			for name, fileData := range tarFiles {
				// We need to fix the relative pathname in the tar file entry.
				// The dirPath we pass in must end with "account-NNNNN" or "NNNNN", but this may also be the first component of the path in the tar entry.
				// If it is, remove it from the tar entry name.
				nameN := strings.Split(name, "/")
				if len(nameN) > 0 && len(pathN) > 0 && nameN[0] == pathN[0] {
					name = strings.Join(nameN[1:], "/")
				}
				filePath := fmt.Sprintf("%s/%s", dirPath, name)
				if tf, err := os.Create(filePath); err == nil {
					defer tf.Close()
					if _, err := tf.Write(fileData); err != nil {
						fmt.Printf("Error writing extracted file [%s] to [%s]\n", name, filePath)
					}
					os.Chtimes(filePath, entry.Modified, entry.Modified);
				} else {
					fmt.Printf("Error creating file: %s\n", err)
				}
			}
			os.Chtimes(dirPath, entry.Modified, entry.Modified)
		}
	}
}

func extractTar(tarData []byte, toDir string) (map[string][]byte, error) {
	buf := bytes.NewBuffer(tarData)
	rslt := make(map[string][]byte)
	tr := tar.NewReader(buf)
	for {
		hdr, err := tr.Next()
		if err == nil {
			if data, err := ioutil.ReadAll(tr); err == nil {
				names := strings.Split(hdr.Name, "/")
				var name string
				if len(names) > 1 {
					name = names[1]
				} else {
					name = names[0]
				}
				if name != "" {
					rslt[name] = data
				}
			}
		} else if err == io.EOF {
			break
		} else {
			return nil, err
		}
	}
	return rslt, nil
}

func loadConfig() *config {
	fmt.Printf("%s: Loading config:\n", time.Now())
	c := &config{}
	if file, err := os.Open("/etc/h2o/h2o.conf"); err == nil {
		defer file.Close()
		scanner := bufio.NewScanner(file)
		scanner.Split(bufio.ScanLines)
		for scanner.Scan() {
			line := scanner.Text()
			nvPair := strings.Split(line, "=")
			if len(nvPair) == 2 {
				switch nvPair[0] {
				case "group_var_env":     // qa7, prod
					c.appEnv = nvPair[1]
				case "region":            // us-east-1
					c.region = nvPair[1]
				case "s3_bucket_prefix":  // vonage-h2o-qa7-va
					c.bucket = nvPair[1]
				case "profile_region":    // va, or
					c.regionID = nvPair[1]
				case "cluster_name":      // call1, call2, ...
					c.appStack = nvPair[1]
				default:
				}
			}
		}
		if c.appStack != "" {
			c.appStack = c.appStack[4:]  // Value is call3, skip "call", keep "3"
		} else {
			c.appStack = "1"
		}
		if c.bucket != "" {
			c.bucket = fmt.Sprintf("%s-dialplan-%s", c.bucket, c.appStack)
		}
	} else {
		c.appEnv = "qa7"
		c.appStack = "2"
		c.bucket = "vonage-h2o-qa7-va-dialplan-2"
		c.region = "us-east-1"
		c.regionID = "va"
	}
	fmt.Printf("%s: Loading config completed:\n", time.Now())
	fmt.Printf("%s: config: appEnv   -> %s\n", time.Now(), c.appEnv)
	fmt.Printf("%s: config: appStack -> %s\n", time.Now(), c.appStack)
	fmt.Printf("%s: config: bucket   -> %s\n", time.Now(), c.bucket)
	fmt.Printf("%s: config: region   -> %s\n", time.Now(), c.region)
	fmt.Printf("%s: config: regionID -> %s\n", time.Now(), c.regionID)
	return c
}



// Credentials holds the various data values that will allow the caller to connect to AWS.
// Not all values need to be present. Should be either the CredFile (with an optional Profile), or
// the AwsKey and AwsSecret.
type Credentials struct {
    CredFile  string
    Profile   string
    AwsKey    string
    AwsSecret string
}

var defaultCredentials = Credentials{}

// DefaultCredentials xxx
func DefaultCredentials(file, profile, key, secret string) {
    defaultCredentials.CredFile = file
    defaultCredentials.Profile = profile
    defaultCredentials.AwsKey = key
    defaultCredentials.AwsSecret = secret
}

// NewSession will create and return a new AWS session object reference by attempting to connect to AWS using credentials.
// If userCred is nil, then the default credentials will be used. Note that those credentials would have had to have been
// set using the function DefaultCredentials(). 
// If authentication fails (or any other error occurs) the underlying error is returned to the caller and the session reference is nil.
func NewSession(region, key, secret, token, file, profile string) (*session.Session, error) {
    if key != "" && secret != "" {
        if sess, err := session.NewSession(&aws.Config{
            Region:      aws.String(region),
            Credentials: credentials.NewStaticCredentials(key, secret, token),
        }); err == nil {
            return sess, nil
        }
    }
    if file != "" {
        if sess, err := session.NewSession(&aws.Config{
            Region:      aws.String(region),
            Credentials: credentials.NewSharedCredentials(file, profile),
        }); err == nil {
            return sess, nil
        }
    }
    if sess, err := session.NewSession(&aws.Config{
        Region: aws.String(region),
    }); err == nil {
        return sess, nil
    } 
    return nil, fmt.Errorf("Cannot authenticate")
}


// s3Entry xxx
type s3Entry struct {
	Region   string
	Bucket   string
	Key      string
	Dir      string
	File     string
	Modified time.Time
}

func s3streamDownload(s3client *s3.S3, bucket, key string) ([]byte, error) {
	buf := &aws.WriteAtBuffer{}
	downloader := s3manager.NewDownloaderWithClient(s3client)
	if _, err := downloader.Download(buf, &s3.GetObjectInput{
		Bucket: &bucket,
		Key:    &key,
	}); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

// s3List xxx
func s3List(s3Session *s3.S3, region, bucket, rawPrefix string) ([]*s3Entry, error) {
	var prefix string //, delim string
	if rawPrefix == "/" || rawPrefix == "" {
		prefix = ""
		//delim = "/"
	} else if strings.HasPrefix(rawPrefix, "/") {
		prefix = rawPrefix[1:]
		//delim = ""
	} else {
		prefix = rawPrefix
		//delim = ""
	}
	if len(prefix) > 0 && !strings.HasSuffix(prefix, "/") {
		prefix = prefix + "/"
	}
	entries := make([]*s3Entry, 0, 50)
	
	readNext  := true
	nextMarker:= ""
	loi := &s3.ListObjectsInput{
		Bucket:	   aws.String(bucket),
		Prefix:    aws.String(prefix),
		//Delimiter: aws.String(delim),
	}
	for readNext {
		if nextMarker != "" {
			loi.Marker = aws.String(nextMarker)
		}
		var lastKey string
		if resp, err := s3Session.ListObjects(loi); err == nil {
			for _, item := range resp.Contents {
				entry := &s3Entry{}
				entry.Key = *item.Key
				lastKey = entry.Key
				// Remove the directory path up to the filename itself.
				if index := strings.LastIndex(entry.Key, "/"); index > -1 {
					entry.Dir  = entry.Key[:index]
					entry.File = entry.Key[index+1:]
				} else {
					entry.Dir  = entry.Key
					entry.File = entry.Key
				}
				entry.Bucket = bucket
				entry.Region = region
				entry.Modified = *item.LastModified

				entries = append(entries, entry)
			}
			if *resp.IsTruncated {
				if resp.NextMarker != nil && len(*resp.NextMarker) > 0 {
					nextMarker = *resp.NextMarker
				} else {
					nextMarker = lastKey
				}
			} else {
				readNext = false
			}
		} else {
			return nil, err
		}
	}
	return entries, nil
}
